import 'package:exercise2/exercise2.dart' as calaverage;
import 'dart:io';
bool isPrime(num) {
  for (var i = 2; i <= num / i; ++i) {
    if (num % i == 0) {
      return false;
    }
  }
  return true;
}

void main() {
  print('Enter num');
  var N = int.parse(stdin.readLineSync()!);
  if (isPrime(N)) {
    print('$N is a prime number.');
  } else {
    print('$N is not a prime number.');
  }
}